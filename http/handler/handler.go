package handler

import (
	"log"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/http/errors"
	"gitlab.com/eclectic2019/delivery/api/http/handler/auth"
	"gitlab.com/eclectic2019/delivery/api/http/handler/deal"
	"gitlab.com/eclectic2019/delivery/api/http/handler/driver"
	"gitlab.com/eclectic2019/delivery/api/http/handler/offer"
	"gitlab.com/eclectic2019/delivery/api/http/handler/order"
	"gitlab.com/eclectic2019/delivery/api/http/handler/pprof"
)

type Config struct {
	*viper.Viper

	AppAuth    domain.AppAuthLayer
	DriverAuth domain.DriverAuthLayer

	Drivers domain.DriverStorage
	Orders  domain.OrderStorage
	Offers  domain.OfferStorage
	Deals   domain.DealStorage
}

func New(config *Config) *echo.Echo {
	e := echo.New()

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "method=${method}, uri=${uri}, status=${status}\n",
	}))

	e.Use(middleware.CORS())

	e.Use(errors.Middleware, errors.Recover)

	e.Use(With(domain.AppAuthContextKey, config.AppAuth))
	e.Use(With(domain.DriverAuthContextKey, config.DriverAuth))

	e.Use(With(domain.DriversContextKey, config.Drivers))
	e.Use(With(domain.OrdersContextKey, config.Orders))
	e.Use(With(domain.OffersContextKey, config.Offers))
	e.Use(With(domain.DealsContextKey, config.Deals))

	if config.GetBool("http.profile") {
		log.Println("Profiling enabled")
		pprof.Route(e)
	}

	// auth by login (phone) and password
	e.POST("/auth", auth.ByPhone)

	// app
	app := e.Group("/app", auth.AppMiddleware)
	app.POST("/orders", order.Create)
	app.PUT("/orders/:id", order.Update)

	// check state of order
	app.GET("/orders/:id", order.One)
	// remove order if no deal
	app.DELETE("/orders/:id", order.Remove)

	// list of drivers sorted by rating
	app.GET("/drivers/:order_id", driver.List)
	// updating driver rating
	app.GET("/deals/:id", deal.Update)

	// driver
	drv := e.Group("/driver", auth.ByTokenMiddleware)

	// list available order for driver
	drv.GET("/orders", order.List)
	// create offer for order
	drv.POST("/orders/:id/offers", offer.Create)
	drv.PUT("/orders/:id/offers/:id", offer.Update)
	drv.DELETE("/orders/:id/offers/:id", offer.Remove)

	// list driver deals
	drv.GET("/deals", deal.List)
	// getting driver profile
	drv.GET("/me", driver.Profile)

	return e
}

func With(name string, v interface{}) echo.MiddlewareFunc {
	return func(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			ctx.Set(name, v)

			return handlerFunc(ctx)
		}
	}
}
