package offer

import (
	"github.com/labstack/echo"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/http/errors"
	"net/http"
)

func Create(ctx echo.Context) error {
	offers, ok := ctx.Get(domain.OffersContextKey).(domain.OfferStorage)
	if !ok {
		return errors.New("offers storage is not defined")
	}

	offer := &domain.Offer{}

	err := ctx.Bind(offer)
	if err != nil {
		return errors.Wrap(err, "binding request body failed")
	}

	err = offers.Create(ctx.Request().Context(), offer)
	if err != nil {
		return errors.Wrap(err, "creating offer failed")
	}

	return ctx.JSON(http.StatusOK, offer)
}
