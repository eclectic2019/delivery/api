package offer

import (
	"github.com/labstack/echo"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/http/errors"
	"strconv"
)

func Remove(ctx echo.Context) error {
	offers, ok := ctx.Get(domain.OffersContextKey).(domain.OfferStorage)
	if !ok {
		return errors.New("offers storage is not defined")
	}

	offerID, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		return errors.BadRequest("bad offer id").WithDetails(err)
	}

	err = offers.Delete(ctx.Request().Context(), &domain.Offer{
		ID: offerID,
	})
	if err != nil {
		return errors.Wrap(err, "deleting offer failed")
	}

	return nil
}
