package pprof

import (
	"net/http"
	"net/http/pprof"

	"github.com/labstack/echo"
)

func Index() echo.HandlerFunc {
	return echo.WrapHandler(http.HandlerFunc(pprof.Index))
}

func Heap() echo.HandlerFunc {
	return echo.WrapHandler(pprof.Handler("heap"))
}

func Goroutine() echo.HandlerFunc {
	return echo.WrapHandler(pprof.Handler("goroutine"))
}

func Block() echo.HandlerFunc {
	return echo.WrapHandler(pprof.Handler("block"))
}

func ThreadCreate() echo.HandlerFunc {
	return echo.WrapHandler(pprof.Handler("threadcreate"))
}

func CmdLine() echo.HandlerFunc {
	return echo.WrapHandler(http.HandlerFunc(pprof.Cmdline))
}

func Profile() echo.HandlerFunc {
	return echo.WrapHandler(http.HandlerFunc(pprof.Profile))
}

func Symbol() echo.HandlerFunc {
	return echo.WrapHandler(http.HandlerFunc(pprof.Symbol))
}

func Route(e *echo.Echo) {
	e.GET("/debug/pprof/", Index())
	e.GET("/debug/pprof/heap", Heap())
	e.GET("/debug/pprof/goroutine", Goroutine())
	e.GET("/debug/pprof/block", Block())
	e.GET("/debug/pprof/cmdline", CmdLine())
	e.GET("/debug/pprof/profile", Profile())
	e.GET("/debug/pprof/symbol", Symbol())
	e.GET("/debug/pprof/threadcreate", ThreadCreate())
}
