package driver

import (
	"strconv"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/http/errors"
)

func List(ctx echo.Context) error {
	drivers, ok := ctx.Get(domain.DriversContextKey).(domain.DriverStorage)
	if !ok {
		return errors.New("driver storage is not defined")
	}

	orders, ok := ctx.Get(domain.OrdersContextKey).(domain.OrderStorage)
	if !ok {
		return errors.New("driver storage is not defined")
	}

	orderID, err := strconv.ParseInt(ctx.Param("order_id"), 10, 64)
	if err != nil {
		return errors.BadRequest("bad order id").WithDetails(err)
	}

	c := ctx.Request().Context()
	order := &domain.Order{
		ID: orderID,
	}

	err = orders.Read(c, order)
	if err != nil {
		return errors.Wrap(err, "getting order failed")
	}

	results, err := drivers.SearchForOrder(c, order)
	if err != nil {
		return errors.Wrap(err, "search for drivers failed")
	}

	return ctx.JSON(http.StatusOK, results)
}
