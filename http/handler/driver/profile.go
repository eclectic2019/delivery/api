package driver

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/http/errors"
)

func Profile(ctx echo.Context) error {
	driver, ok := ctx.Get(domain.DriverUserKey).(*domain.Driver)
	if !ok || driver == nil {
		return errors.New("driver account is not defined")
	}

	return ctx.JSON(http.StatusOK, driver)
}