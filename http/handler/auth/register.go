package auth

import (
	"github.com/labstack/echo"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/http/errors"
)

func Register(ctx echo.Context) error {
	drivers, ok := ctx.Get(domain.DriversContextKey).(domain.DriverStorage)
	if !ok {
		return errors.New("driver storage is not defined")
	}

	driver := &domain.Driver{}

	err := ctx.Bind(driver)
	if err != nil {
		return errors.Wrap(err, "binding request body failed")
	}

	c := ctx.Request().Context()

	err = drivers.Create(c, driver)
	if err != nil {
		return errors.Wrap(err, "creating driver failed")
	}

	return nil
}