package auth

import (
	"github.com/labstack/echo"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/http/errors"
)

var AppAuthTokenKey = "X-App-Auth-Token"

func AppMiddleware(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		authLayer, ok := ctx.Get(domain.AppAuthContextKey).(domain.AppAuthLayer)
		if !ok {
			return errors.New("auth layer is not defined")
		}

		token := ctx.Request().Header.Get(AppAuthTokenKey)

		app, err := authLayer.Auth(ctx.Request().Context(), token)
		if err != nil {
			return errors.Wrap(err, "getting app failed")
		}
		if app == nil {
			return errors.Forbidden("app is not found")
		}

		return handlerFunc(ctx)
	}
}
