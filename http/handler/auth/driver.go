package auth

import (
	"time"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/http/errors"
)

func ByPhone(ctx echo.Context) error {
	authLayer, ok := ctx.Get(domain.DriverAuthContextKey).(domain.DriverAuthLayer)
	if !ok {
		return errors.New("auth layer is not defined")
	}

	drivers, ok := ctx.Get(domain.DriversContextKey).(domain.DriverStorage)
	if !ok {
		return errors.New("driver storage is not defined")
	}

	req := &domain.Driver{}

	err := ctx.Bind(req)
	if err != nil {
		return errors.Wrap(err, "binding request body failed")
	}

	c := ctx.Request().Context()

	driver, err := drivers.GetByPhone(c, req.Phone)
	if err != nil {
		return errors.Wrap(err, "search driver account failed")
	}
	if driver == nil {
		return errors.NotFound("driver account not failed")
	}

	driver.AuthCode = req.AuthCode

	token, created, err := authLayer.Auth(c, driver)
	if err != nil {
		return errors.Wrap(err, "auth failed")
	}
	if token == nil || created == nil {
		return errors.Unauthorized("invalid phone or auth code")
	}

	return ctx.JSON(http.StatusOK, &response{
		Token:     *token,
		DTCreated: *created,
	})
}

type response struct {
	Token     string    `json:"token"`
	DTCreated time.Time `json:"dt_created"`
}

var DriverAuthTokenKey = "X-Auth-Token"

func ByTokenMiddleware(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		authLayer, ok := ctx.Get(domain.DriverAuthContextKey).(domain.DriverAuthLayer)
		if !ok {
			return errors.New("auth layer is not defined")
		}

		token := ctx.Request().Header.Get(DriverAuthTokenKey)

		driver, err := authLayer.AuthByToken(ctx.Request().Context(), token)
		if err != nil {
			return errors.Wrap(err, "auth by token failed")
		}
		if driver == nil {
			return errors.Forbidden("driver account not found")
		}

		ctx.Set(domain.DriverUserKey, driver)

		return handlerFunc(ctx)
	}
}
