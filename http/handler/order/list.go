package order

import (
	"strconv"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/http/errors"
)

func List(ctx echo.Context) error {
	orders, ok := ctx.Get(domain.OrdersContextKey).(domain.OrderStorage)
	if !ok {
		return errors.New("driver storage is not defined")
	}

	c := ctx.Request().Context()

	filter := &domain.OrderFilter{}

	driver, ok := ctx.Get(domain.DriverUserKey).(*domain.Driver)
	if ok {
		filter.Driver = driver
	}

	if lim := ctx.QueryParam("limit"); lim != "" {
		limit, err := strconv.Atoi(lim)
		if err != nil {
			return errors.BadRequest("bad limit").WithDetails(err)
		}

		filter.Limit = limit

		if off := ctx.QueryParam("offset"); off != "" {
			offset, err := strconv.Atoi(off)
			if err != nil {
				return errors.BadRequest("bad offset").WithDetails(err)
			}

			filter.Offset = offset
		}
	}


	results, err := orders.List(c, filter)
	if err != nil {
		return errors.Wrap(err, "getting orders failed")
	}

	return ctx.JSON(http.StatusOK, results)
}