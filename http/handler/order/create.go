package order

import (
	"github.com/labstack/echo"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/http/errors"
	"net/http"
)

func Create(ctx echo.Context) error {
	orders, ok := ctx.Get(domain.OrdersContextKey).(domain.OrderStorage)
	if !ok {
		return errors.New("orders storage is undefined")
	}

	order := &domain.Order{}

	err := ctx.Bind(order)
	if err != nil {
		return errors.Wrap(err, "binding request body failed")
	}

	c := ctx.Request().Context()

	err = orders.Create(c, order)
	if err != nil {
		return errors.Wrap(err, "creating order failed")
	}

	return ctx.JSON(http.StatusOK, order)
}
