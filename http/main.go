package main

import (
	"database/sql"
	"log"
	"os"
	"time"

	_ "github.com/lib/pq"

	"github.com/getsentry/raven-go"
	"github.com/pkg/errors"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/eclectic2019/delivery/api/http/handler"
	"gitlab.com/eclectic2019/delivery/api/layers/postgres"
)

var (
	confPath     = pflag.String("config", "./config.yaml", "Configuration file")
	connLifetime = pflag.Duration("conn_lifetime", time.Second, "postgres connection lifetime")
)

func main() {
	config, err := readConfig(confPath)
	if err != nil {
		log.Fatalf("config error: %s", err)
	}

	log.Println(config.AllKeys())

	config.BindEnv("http.host", "HTTP_HOST")
	config.BindEnv("http.port", "HTTP_PORT")
	config.BindEnv("database.driver", "DB_DRIVER")
	config.BindEnv("database.prod.dsn", "DB_PROD_DSN")
	config.BindEnv("sentry.dsn", "SENTRY_DSN")

	if sentryDSN := config.GetString("sentry.dsn"); sentryDSN != "" {
		err = raven.SetDSN(sentryDSN)
		if err != nil {
			log.Fatalf("sentry error: %s", err)
		}

		log.Println("sentry initialized")
	}

	db, err := sql.Open(config.GetString("database.driver"), config.GetString("database.prod.dsn"))
	if err != nil {
		log.Fatalf("database error: %s", err)
	}
	db.SetConnMaxLifetime(*connLifetime)

	h := handler.New(&handler.Config{
		Viper: config,

		AppAuth:    postgres.AppAuthLayer(db),
		DriverAuth: postgres.DriverStorage(db),

		Drivers: postgres.DriverStorage(db),
		Orders:  postgres.OrderStorage(db),
		Offers:  postgres.OfferStorage(db),
		Deals:   postgres.DealStorage(db),
	})

	addr := config.GetString("http.host") + ":" + config.GetString("http.port")

	h.Logger.Fatal(h.Start(addr))
}

func readConfig(path *string) (*viper.Viper, error) {
	config := viper.New()
	config.SetConfigType("yaml")

	if *path != "" {
		log.Println(*path)
		configFile, err := os.Open(*path)
		if err != nil {
			return nil, errors.Wrap(err, "opening config failed")
		}

		err = config.ReadConfig(configFile)
		if err != nil {
			return nil, errors.Wrap(err, "reading config failed")
		}
	}

	return config, nil
}
