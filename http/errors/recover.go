package errors

import (
	"runtime"
	"fmt"
	"log"

	"github.com/labstack/echo"
	"github.com/getsentry/raven-go"
)

func Recover(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		defer func() {
			if rec := recover(); rec != nil {
				s := make([]byte, 4<<10)
				l := runtime.Stack(s, false)

				err := fmt.Errorf("recovered from %v with stack: %s path: %s", rec, s[0:l], ctx.Path())
				raven.CaptureError(err, nil)
				log.Println(err)
			}
		}()

		return handlerFunc(ctx)
	}
}
