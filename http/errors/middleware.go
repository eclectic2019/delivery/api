package errors

import (
	"github.com/labstack/echo"
	"github.com/getsentry/raven-go"
)

func Middleware(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		err := handlerFunc(ctx)

		e, ok := err.(*HttpError)

		if ok {
			return ctx.JSON(e.Code(), e)
		}

		if err != nil {
			e = Internal("Internal error").WithDetails(err)
			raven.CaptureError(err, nil)
			ctx.Logger().Error(e)
			return ctx.JSON(e.Code(), e)
		}

		return nil
	}
}
