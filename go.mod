module gitlab.com/eclectic2019/delivery/api

go 1.12

require (
	github.com/certifi/gocertifi v0.0.0-20190506164543-d2eda7129713 // indirect
	github.com/ericlagergren/decimal v0.0.0-20190714012041-d0a435755ae8 // indirect
	github.com/ernsheong/grand v0.0.0-20171208035557-4a4ac72dc1de
	github.com/getsentry/raven-go v0.2.0
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/kat-co/vala v0.0.0-20170210184112-42e1d8b61f12
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.2.9 // indirect
	github.com/lib/pq v1.1.1
	github.com/partyzanex/layer v0.0.1
	github.com/pkg/errors v0.8.1
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.4.0
	github.com/volatiletech/inflect v0.0.0-20170731032912-e7201282ae8d // indirect
	github.com/volatiletech/null v8.0.0+incompatible
	github.com/volatiletech/sqlboiler v3.4.0+incompatible
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
)
