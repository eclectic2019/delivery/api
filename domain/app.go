package domain

import (
	"time"
	"context"
)

type App struct {
	ID          int     `json:"id"`
	Status      AppStatus `json:"status"`
	AccessToken      string    `json:"secret"`
	DTLastUsage time.Time `json:"dt_last_usage"`
}

type AppStatus string

const (
	Active      AppStatus = "active"
	Blocked     AppStatus = "blocked"

	AppAuthContextKey = "app_auth"
)

type AppAuthLayer interface {
	// search App by access token and update date of last usage
	Auth(ctx context.Context, accessToken string) (*App, error)
}
