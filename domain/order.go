package domain

import (
	"context"
	"time"
)

type Order struct {
	ID       int64       `json:"id"`
	Route    []Location  `json:"route"`
	PriceMin int64       `json:"price_min"`
	PriceMax int64       `json:"price_max"`
	Price    int64       `json:"price"`
	Weight   int64       `json:"weight"`
	Desc     string      `json:"desc"`
	Status   OrderStatus `json:"status"`

	DTStart time.Time `json:"dt_start"`
	DTEnd   time.Time `json:"dt_end"`

	Offers []*Offer `json:"offers,omitempty"`
	Deals  []*Deal  `json:"deal,omitempty"`
}

type OrderStatus string

const (
	NewOrder       OrderStatus = "new"
	AcceptedOrder  OrderStatus = "accepted"
	ConfirmedOrder OrderStatus = "confirmed"
	InProgessOrder OrderStatus = "in_progress"
	CompletedOrder OrderStatus = "completed"

	OrdersContextKey = "orders"
)

type Location struct {
	*Region

	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
	Address string  `json:"address"`
}

type Region struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type RegionStorage interface {
	List(ctx context.Context) ([]*Region, error)
}

type OrderFilter struct {
	Driver        *Driver
	Limit, Offset int
}

type OrderStorage interface {
	List(ctx context.Context, filter *OrderFilter) ([]*Order, error)
	Create(ctx context.Context, order *Order) error
	Read(ctx context.Context, order *Order) error
	Update(ctx context.Context, order *Order) error
	Remove(ctx context.Context, order *Order) error
}
