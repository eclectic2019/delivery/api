package domain

import (
	"context"
	"time"
)

type Driver struct {
	ID     int64   `json:"id"`
	Phone  string  `json:"phone"`
	Rating float64 `json:"rating"`

	// additional data fields
	Profile   map[string]string `json:"profile"`
	Transport *Transport        `json:"transport"`
	Location  *Location         `json:"location"`

	AuthCode *string `json:"auth_code,omitempty"`
}

type Transport struct {
	ID       int64   `json:"id"`
	Name     string  `json:"name"`
	Cost     int64   `json:"cost"`
	Tariff   int64   `json:"tariff"`
	Carrying float64 `json:"carrying"`
}

const (
	DriversContextKey = "drivers"
	DriverUserKey = "driver"
)

type DriverStorage interface {
	GetByID(ctx context.Context, id int64) (*Driver, error)
	GetByPhone(ctx context.Context, phone string) (*Driver, error)

	SearchForOrder(ctx context.Context, order *Order) ([]*Driver, error)
	Create(ctx context.Context, driver *Driver) error
	Update(ctx context.Context, driver *Driver) error
}

// todo: remove from prod
const TestAuthCode = "1234"
const DriverAuthContextKey = "driver_auth"

type DriverAuthLayer interface {
	Auth(ctx context.Context, driver *Driver) (accessToken *string, DTCreated *time.Time, err error)
	AuthByToken(ctx context.Context, accessToken string) (*Driver, error)
}
