package domain

import (
	"context"
	"time"
)

// todo: add status
type Offer struct {
	ID        int64     `json:"id"`
	Price     int64     `json:"price"`
	DTCreated time.Time `json:"dt_created"`
	DTUpdated time.Time `json:"dt_updated"`

	Order  *Order  `json:"order"`
	Driver *Driver `json:"driver"`
}

const OffersContextKey = "offers"

type OfferStorage interface {
	SearchByOrderID(ctx context.Context, orderID int64) ([]*Offer, error)
	Create(ctx context.Context, offer *Offer) error
	Update(ctx context.Context, offer *Offer) error
	Delete(ctx context.Context, offer *Offer) error
}
