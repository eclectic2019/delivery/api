package domain

import (
	"context"
	"time"
)

type Deal struct {
	ID        int64     `json:"id"`
	Rating    float64   `json:"rating"`
	Price     int64     `json:"price"`
	DTCreated time.Time `json:"dt_created"`
	DTUpdated time.Time `json:"dt_updated"`
	Completed bool      `json:"completed"`

	Driver *Driver `json:"driver"`
	Offer  *Offer  `json:"offer"`
	Order  *Order  `json:"order"`
}

const DealsContextKey = "deals"

type DealStorage interface {
	SearchByOrderID(ctx context.Context, orderID int64) ([]*Deal, error)
	SearchByDriverID(ctx context.Context, orderID int64) ([]*Deal, error)
	Create(ctx context.Context, deal *Deal) error
	Update(ctx context.Context, deal *Deal) error
	Delete(ctx context.Context, deal *Deal) error
}
