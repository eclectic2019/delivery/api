FROM golang:1.12.6-alpine3.9 as builder

RUN apk add --no-cache openssh-client git

COPY . /go/src/gitlab.com/app-builder/backoffice/api.git
WORKDIR /go/src/gitlab.com/app-builder/backoffice/api.git

ENV CGO_ENABLED 0
ENV GO111MODULE on

RUN go build -v -o /api ./http/main.go



FROM alpine:3.9

EXPOSE 8787

WORKDIR /app

COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /usr/local/go/lib/time/zoneinfo.zip
COPY --from=builder /api /app/api

RUN apk --update add tzdata ca-certificates && \
  cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
  echo "Europe/Moscow" > /etc/timezone && \
  date && \
  apk del tzdata

ENTRYPOINT ["./api"]
