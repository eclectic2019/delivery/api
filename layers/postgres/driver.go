package postgres

import (
	"context"
	"fmt"
	"sync"

	"github.com/partyzanex/layer"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/models/postgres"
	"time"
	"golang.org/x/crypto/bcrypt"
	"github.com/ernsheong/grand"
	"database/sql"
)

type driverStorage struct {
	ex layer.BoilExecutor
}

func (srv *driverStorage) GetByID(ctx context.Context, id int64) (*domain.Driver, error) {
	c, ex := layer.GetExecutor(ctx, srv.ex)

	model, err := postgres.Drivers(qm.Where("id = ?", id)).One(c, ex)
	if err != nil {
		return nil, errors.Wrap(err, "getting driver by id failed")
	}

	return modelToDriver(model, nil), nil
}

func (srv *driverStorage) GetByPhone(ctx context.Context, phone string) (*domain.Driver, error) {
	c, ex := layer.GetExecutor(ctx, srv.ex)

	model, err := postgres.Drivers(qm.Where("phone = ?", phone)).One(c, ex)
	if err != nil {
		return nil, errors.Wrap(err, "getting driver by phone failed")
	}

	return modelToDriver(model, nil), nil
}

func (srv *driverStorage) SearchForOrder(ctx context.Context, order *domain.Order) ([]*domain.Driver, error) {
	if order == nil {
		return nil, nil
	}

	clause := fmt.Sprintf(
		`id in (select driver_id from %s where carrying <= ?)`,
		postgres.TableNames.Transport,
	)

	mods := []qm.QueryMod{
		qm.Where(clause, order.Weight),
		qm.OrderBy("rating DESC"),
	}

	c, ex := layer.GetExecutor(ctx, srv.ex)

	models, err := postgres.Drivers(mods...).All(c, ex)
	if err != nil {
		return nil, errors.Wrap(err, "getting driver by phone failed")
	}

	drivers := make([]*domain.Driver, len(models))

	for i, model := range models {
		drivers[i] = modelToDriver(model, nil)
	}

	return drivers, nil
}

func (srv *driverStorage) Update(ctx context.Context, driver *domain.Driver) error {
	model := &postgres.Driver{
		ID:    driver.ID,
		Phone: driver.Phone,
	}

	model.Rating.SetFloat64(driver.Rating)
	model.Lat.SetFloat64(driver.Location.Lat)
	model.Lon.SetFloat64(driver.Location.Lon)

	tx, err := srv.ex.BeginTx(ctx, nil)
	if err != nil {
		return errors.Wrap(err, "creating transaction failed")
	}

	defer layer.ExecuteTransaction(tx, err)

	err = model.Profile.Marshal(driver.Profile)
	if err != nil {
		return errors.Wrap(err, "marshaling driver profile failed")
	}

	_, err = model.Update(ctx, tx, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "updating driver failed")
	}

	if driver.Transport != nil {
		t := &postgres.Transport{
			ID:       driver.Transport.ID,
			Name:     driver.Transport.Name,
			DriverID: model.ID,
			Tariff:   driver.Transport.Tariff,
			Cost:     driver.Transport.Tariff,
		}

		t.Carryng.SetFloat64(driver.Transport.Carrying)

		err = t.Upsert(ctx, tx, true, nil, boil.Infer(), boil.Infer())
		if err != nil {
			return errors.Wrap(err, "updating transport failed")
		}
	}

	return nil
}

func (srv *driverStorage) Create(ctx context.Context, driver *domain.Driver) error {
	model := &postgres.Driver{
		ID:    driver.ID,
		Phone: driver.Phone,
	}

	model.Rating.SetFloat64(driver.Rating)
	model.Lat.SetFloat64(driver.Location.Lat)
	model.Lon.SetFloat64(driver.Location.Lon)

	tx, err := srv.ex.BeginTx(ctx, nil)
	if err != nil {
		return errors.Wrap(err, "creating transaction failed")
	}

	defer layer.ExecuteTransaction(tx, err)

	err = model.Profile.Marshal(driver.Profile)
	if err != nil {
		return errors.Wrap(err, "marshaling driver profile failed")
	}

	err = model.Insert(ctx, tx, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "inserting driver failed")
	}

	if driver.Transport != nil {
		t := &postgres.Transport{
			ID:       driver.Transport.ID,
			Name:     driver.Transport.Name,
			DriverID: model.ID,
			Tariff:   driver.Transport.Tariff,
			Cost:     driver.Transport.Tariff,
		}

		t.Carryng.SetFloat64(driver.Transport.Carrying)

		err = t.Upsert(ctx, tx, true, nil, boil.Infer(), boil.Infer())
		if err != nil {
			return errors.Wrap(err, "updating transport failed")
		}
	}

	return nil
}

func (srv *driverStorage) Auth(ctx context.Context, driver *domain.Driver) (accessToken *string, DTCreated *time.Time, err error) {
	if driver.Phone == "" {
		return nil, nil, errors.New("phone is required")
	}

	if driver.AuthCode == nil || *driver.AuthCode == "" {
		return nil, nil, errors.New("auth code is required")
	}

	// todo: remove from prod
	if *driver.AuthCode != domain.TestAuthCode {
		return nil, nil, errors.New("invalid auth code")
	}

	result, err := srv.GetByPhone(ctx, driver.Phone)
	if err != nil {
		return nil, nil, err
	}

	str := grand.GenerateRandomString(64)

	b, err := bcrypt.GenerateFromPassword([]byte(str), bcrypt.DefaultCost)
	if err != nil {
		return nil, nil, errors.Wrap(err, "bcrypt error")
	}

	model := &postgres.DriverToken{
		DriverID:    result.ID,
		DTCreated:   time.Now(),
		AccessToken: string(b),
	}

	c, ex := layer.GetExecutor(ctx, srv.ex)

	err = model.Insert(c, ex, boil.Infer())
	if err != nil {
		return nil, nil, errors.Wrap(err, "creating token failed")
	}

	return &model.AccessToken, &model.DTCreated, nil
}

func (srv *driverStorage) AuthByToken(ctx context.Context, accessToken string) (*domain.Driver, error) {
	c, ex := layer.GetExecutor(ctx, srv.ex)

	model, err := postgres.DriverTokens(
		qm.Where("access_token = ?", accessToken),
		qm.Load("Driver.Transports"),
	).One(c, ex)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "search token failed")
	}

	return modelToDriver(model.R.Driver, nil), nil
}

func modelToDriver(model *postgres.Driver, driver *domain.Driver) *domain.Driver {
	if driver == nil {
		driver = &domain.Driver{}
	}

	driver.ID = model.ID

	lon, _ := model.Lon.Float64()
	lat, _ := model.Lat.Float64()
	driver.Location = &domain.Location{
		Lon: lon,
		Lat: lat,
	}

	profile := make(map[string]string)
	model.Profile.Unmarshal(&profile)

	driver.Profile = profile

	if model.R != nil {
		if model.R.Transports != nil {
			for _, transport := range model.R.Transports {
				driver.Transport = modelToTransport(transport, nil)
				break
			}
		}
	}

	driver.Phone = model.Phone
	driver.Rating, _ = model.Rating.Float64()

	return driver
}

func modelToTransport(model *postgres.Transport, transport *domain.Transport) *domain.Transport {
	if transport == nil {
		transport = &domain.Transport{}
	}

	transport.ID = model.ID
	transport.Name = model.Name
	transport.Carrying, _ = model.Carryng.Float64()
	transport.Cost = model.Cost
	transport.Tariff = model.Tariff

	return transport
}

var driverOnce sync.Once

var driverInst *driverStorage

func DriverStorage(ex layer.BoilExecutor) *driverStorage {
	driverOnce.Do(func() {
		driverInst = &driverStorage{
			ex: ex,
		}
	})

	return driverInst
}
