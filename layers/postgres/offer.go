package postgres

import (
	"context"
	"sync"
	"time"

	"github.com/partyzanex/layer"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/models/postgres"
)

type offerStorage struct {
	ex layer.BoilExecutor
}

func (srv *offerStorage) SearchByOrderID(ctx context.Context, orderID int64) ([]*domain.Offer, error) {
	c, ex := layer.GetExecutor(ctx, srv.ex)

	models, err := postgres.Offers(qm.Where("id = ?", orderID)).All(c, ex)
	if err != nil {
		return nil, errors.Wrap(err, "getting offer failed")
	}

	offers := make([]*domain.Offer, len(models))

	for i, model := range models {
		offers[i] = modelToOffer(model, nil)
	}

	return offers, nil
}

func (srv *offerStorage) Create(ctx context.Context, offer *domain.Offer) error {
	model := offerToModel(offer)

	c, ex := layer.GetExecutor(ctx, srv.ex)

	err := model.Insert(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "inserting offer failed")
	}

	modelToOffer(model, offer)
	return nil
}

func (srv *offerStorage) Update(ctx context.Context, offer *domain.Offer) error {
	model := offerToModel(offer)

	model.DTUpdated = time.Now()

	c, ex := layer.GetExecutor(ctx, srv.ex)

	_, err := model.Update(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "updating offer failed")
	}

	modelToOffer(model, offer)
	return nil
}

func (srv *offerStorage) Delete(ctx context.Context, offer *domain.Offer) error {
	model := offerToModel(offer)

	c, ex := layer.GetExecutor(ctx, srv.ex)

	_, err := model.Delete(c, ex)
	if err != nil {
		return errors.Wrap(err, "deleting offer failed")
	}

	return nil
}

func offerToModel(offer *domain.Offer) *postgres.Offer {
	model := &postgres.Offer{
		ID:        offer.ID,
		Price:     offer.Price,
		DriverID:  offer.Driver.ID,
		OrderID:   offer.Order.ID,
		DTCreated: offer.DTCreated,
		DTUpdated: offer.DTUpdated,
	}

	return model
}

func modelToOffer(model *postgres.Offer, offer *domain.Offer) *domain.Offer {
	if offer == nil {
		offer = &domain.Offer{}
	}

	offer.ID = model.ID
	offer.Price = model.Price
	offer.DTCreated = model.DTCreated
	offer.DTUpdated = model.DTUpdated

	if model.R != nil {
		if model.R.Order != nil {
			offer.Order = modelToOrder(model.R.Order, nil)
		}

		if model.R.Driver != nil {
			offer.Driver = modelToDriver(model.R.Driver, nil)
		}
	}

	return offer
}

var offerOnce sync.Once

var offerInst *offerStorage

func OfferStorage(ex layer.BoilExecutor) *offerStorage {
	offerOnce.Do(func() {
		offerInst = &offerStorage{
			ex: ex,
		}
	})

	return offerInst
}
