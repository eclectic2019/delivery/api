package postgres

import (
	"time"
	"context"
	"database/sql"

	"github.com/partyzanex/layer"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/models/postgres"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"sync"
	"crypto/md5"
	"fmt"
)

type appAuthLayer struct {
	ex layer.BoilExecutor
}

func (srv *appAuthLayer) Auth(ctx context.Context, accessToken string) (*domain.App, error) {
	tx, err := srv.ex.BeginTx(ctx, nil)
	if err != nil {
		return nil, errors.Wrap(err, "creating transaction failed")
	}

	defer layer.ExecuteTransaction(tx, err)

	token := md5.Sum([]byte(accessToken))

	model, err := postgres.Apps(qm.Where("MD5(access_token) = ?", fmt.Sprintf("%x", token))).One(ctx, tx)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "search for app failed")
	}

	model.DTLastUsage.Valid = true
	model.DTLastUsage.Time = time.Now()

	_, err = model.Update(ctx, tx, boil.Whitelist(postgres.AppColumns.DTLastUsage))
	if err != nil {
		return nil, errors.Wrap(err, "updating date of last usage failed")
	}

	return &domain.App{
		ID:          model.ID,
		Status:      domain.AppStatus(model.Status),
		AccessToken: model.AccessToken,
		DTLastUsage: model.DTLastUsage.Time,
	}, nil
}

var appOnce sync.Once

var appInst *appAuthLayer

func AppAuthLayer(ex layer.BoilExecutor) *appAuthLayer {
	appOnce.Do(func() {
		appInst = &appAuthLayer{
			ex: ex,
		}
	})

	return appInst
}
