package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"sync"

	"github.com/partyzanex/layer"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/models/postgres"
)

type orderStorage struct {
	ex layer.BoilExecutor
}

func (srv *orderStorage) List(ctx context.Context, filter *domain.OrderFilter) ([]*domain.Order, error) {
	mods := []qm.QueryMod{
		qm.Load("Offers"),
		qm.Load("OrderDeals"),
	}

	if filter != nil {
		if filter.Driver != nil {
			cause := fmt.Sprintf(
				`id in (select order_id from %s od where driver_id = ?)`,
				postgres.TableNames.OrderDeal,
			)

			mods = append(mods, qm.Where(cause, filter.Driver.ID))
		}

		if filter.Limit > 0 {
			mods = append(mods, qm.Limit(filter.Limit))

			if filter.Offset >= 0 {
				mods = append(mods, qm.Offset(filter.Offset))
			}
		}
	}

	c, ex := layer.GetExecutor(ctx, srv.ex)

	models, err := postgres.Orders(mods...).All(c, ex)
	if err != nil && err != sql.ErrNoRows {
		return nil, errors.Wrap(err, "getting order failed")
	}

	orders := make([]*domain.Order, len(models))

	for i, model := range models {
		orders[i] = modelToOrder(model, nil)
	}

	return orders, nil
}

func (srv *orderStorage) Create(ctx context.Context, order *domain.Order) error {
	model := orderToModel(order)

	c, ex := layer.GetExecutor(ctx, srv.ex)

	err := model.Insert(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "inserting order failed")
	}

	modelToOrder(model, order)
	return nil
}

func (srv *orderStorage) Read(ctx context.Context, order *domain.Order) error {
	c, ex := layer.GetExecutor(ctx, srv.ex)

	mods := []qm.QueryMod{
		qm.Load("Offers"),
		qm.Load("OrderDeals"),
		qm.Where("id = ?", order.ID),
	}

	model, err := postgres.Orders(mods...).One(c, ex)
	if err != nil {
		return errors.Wrap(err, "getting order failed")
	}

	modelToOrder(model, order)
	return nil
}

func (srv *orderStorage) Update(ctx context.Context, order *domain.Order) error {
	model := orderToModel(order)

	c, ex := layer.GetExecutor(ctx, srv.ex)

	_, err := model.Update(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "updating order failed")
	}

	modelToOrder(model, order)
	return nil
}

func (srv *orderStorage) Remove(ctx context.Context, order *domain.Order) error {
	c, ex := layer.GetExecutor(ctx, srv.ex)

	model, err := postgres.Orders(qm.Where("id = ?", order.ID)).One(c, ex)
	if err != nil {
		return errors.Wrap(err, "getting order failed")
	}

	_, err = model.Delete(c, ex)
	if err != nil {
		return errors.Wrap(err, "deleting order failed")
	}

	return nil
}

func modelToOrder(model *postgres.Order, order *domain.Order) *domain.Order {
	if order == nil {
		order = &domain.Order{}
	}

	order.ID = model.ID
	order.Status = domain.OrderStatus(model.Status)

	var route []domain.Location
	model.Route.Unmarshal(&route)

	order.Weight = model.Weight
	order.Desc = model.Desc

	order.Route = route
	order.PriceMin = model.PriceMin
	order.PriceMax = model.PriceMax
	order.DTEnd = model.DTEnd.Time
	order.DTStart = model.DTStart.Time

	if model.R != nil {
		if model.R.Offers != nil {
			order.Offers = make([]*domain.Offer, len(model.R.Offers))
			for i, offer := range model.R.Offers {
				order.Offers[i] = modelToOffer(offer, nil)
			}
		}

		if model.R.OrderDeals != nil {
			order.Deals = make([]*domain.Deal, len(model.R.OrderDeals))
			for i, deal := range model.R.OrderDeals {
				order.Deals[i] = modelToDeal(deal, nil)
			}
		}
	}

	return order
}

func orderToModel(order *domain.Order) *postgres.Order {
	model := &postgres.Order{}

	model.ID = order.ID
	model.Status = string(order.Status)
	model.Route.Marshal(order.Route)
	model.PriceMin = order.PriceMax
	model.PriceMax = order.PriceMin
	model.Desc = order.Desc
	model.Weight = order.Weight
	model.DTEnd.Valid = !order.DTEnd.IsZero()
	model.DTEnd.Time = order.DTEnd
	model.DTStart.Valid = !order.DTStart.IsZero()
	model.DTStart.Time = order.DTStart

	return model
}

var orderOnce sync.Once

var orderInst *orderStorage

func OrderStorage(ex layer.BoilExecutor) *orderStorage {
	orderOnce.Do(func() {
		orderInst = &orderStorage{
			ex: ex,
		}
	})

	return orderInst
}
