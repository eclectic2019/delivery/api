package postgres

import (
	"context"
	"database/sql"
	"sync"

	"github.com/partyzanex/layer"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/eclectic2019/delivery/api/domain"
	"gitlab.com/eclectic2019/delivery/api/models/postgres"
)

type dealStorage struct {
	ex layer.BoilExecutor
}

func (srv *dealStorage) SearchByOrderID(ctx context.Context, orderID int64) ([]*domain.Deal, error) {
	return srv.search(ctx, qm.Where("order_id = ?", orderID))
}

func (srv *dealStorage) SearchByDriverID(ctx context.Context, driverID int64) ([]*domain.Deal, error) {
	return srv.search(ctx, qm.Where("driver_id = ?", driverID))
}

func (srv *dealStorage) search(ctx context.Context, mods ...qm.QueryMod) ([]*domain.Deal, error) {
	c, ex := layer.GetExecutor(ctx, srv.ex)

	models, err := postgres.OrderDeals(mods...).All(c, ex)
	if err != nil && err != sql.ErrNoRows {
		return nil, errors.Wrap(err, "getting deals failed")
	}

	deals := make([]*domain.Deal, len(models))

	for i, model := range models {
		deals[i] = modelToDeal(model, nil)
	}

	return deals, nil
}

func (srv *dealStorage) Create(ctx context.Context, deal *domain.Deal) error {
	model := dealToModel(deal)

	c, ex := layer.GetExecutor(ctx, srv.ex)

	err := model.Insert(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "inserting deal failed")
	}

	modelToDeal(model, deal)
	return nil
}

func (srv *dealStorage) Update(ctx context.Context, deal *domain.Deal) error {
	model := dealToModel(deal)

	c, ex := layer.GetExecutor(ctx, srv.ex)

	_, err := model.Update(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, "updating deal failed")
	}

	modelToDeal(model, deal)
	return nil
}

func (srv *dealStorage) Delete(ctx context.Context, deal *domain.Deal) error {
	model := dealToModel(deal)

	c, ex := layer.GetExecutor(ctx, srv.ex)

	_, err := model.Delete(c, ex)
	if err != nil {
		return errors.Wrap(err, "deleting deal failed")
	}

	return nil
}

func modelToDeal(model *postgres.OrderDeal, deal *domain.Deal) *domain.Deal {
	if deal == nil {
		deal = &domain.Deal{}
	}

	deal.ID = model.ID
	deal.Price = model.Price
	deal.Rating, _ = model.Rating.Float64()
	deal.DTCreated = model.DTCreated
	deal.DTUpdated = model.DTUpdated
	deal.Completed = model.Completed

	if model.R != nil {
		if model.R.Order != nil {
			deal.Order = modelToOrder(model.R.Order, nil)
		}

		if model.R.Driver != nil {
			deal.Driver = modelToDriver(model.R.Driver, nil)
		}

		if model.R.Offer != nil {
			deal.Offer = modelToOffer(model.R.Offer, nil)
		}
	}

	return nil
}

func dealToModel(deal *domain.Deal) *postgres.OrderDeal {
	model := &postgres.OrderDeal{
		ID:        deal.ID,
		Price:     deal.Price,
		Completed: deal.Completed,
		DTCreated: deal.DTCreated,
		DTUpdated: deal.DTUpdated,

		OrderID:  deal.Order.ID,
		DriverID: deal.Driver.ID,
		OfferID:  deal.Offer.ID,
	}

	return model
}

var dealOnce sync.Once

var dealInst *dealStorage

func DealStorage(ex layer.BoilExecutor) *dealStorage {
	dealOnce.Do(func() {
		dealInst = &dealStorage{
			ex: ex,
		}
	})

	return dealInst
}
