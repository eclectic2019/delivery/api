-- +mig Up

CREATE TABLE public.driver_token
(
  id BIGSERIAL NOT NULL,
  driver_id BIGINT NOT NULL,
  access_token CHARACTER VARYING (64) NOT NULL,
  dt_created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  dt_last_usage TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,

  CONSTRAINT driver_token_pkey PRIMARY KEY (id),
  CONSTRAINT fk_token_driver_id FOREIGN KEY (driver_id)
  REFERENCES public.driver (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE
)
WITH (
OIDS = FALSE
);

-- +mig Down

DROP TABLE public.driver_token CASCADE;

