-- +mig Up

CREATE TABLE public.order
(
  id        BIGSERIAL             NOT NULL,
  route     JSONB                 NOT NULL,
  price_min BIGINT                NOT NULL,
  price_max BIGINT                NOT NULL,
  status    CHARACTER VARYING(16) NOT NULL DEFAULT 'new',
  weight    BIGINT                NOT NULL,
  "desc"    CHARACTER VARYING     NOT NULL,
  dt_start  TIMESTAMP WITHOUT TIME ZONE,
  dt_end    TIMESTAMP WITHOUT TIME ZONE,

  CONSTRAINT order_pkey PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.region
(
  id   BIGSERIAL NOT NULL,
  name CHARACTER VARYING(255),
  CONSTRAINT region_pkey PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.driver
(
  id      BIGSERIAL              NOT NULL,
  phone   CHARACTER VARYING(255) NOT NULL,
  profile JSONB                  NOT NULL,
  rating  NUMERIC                NOT NULL DEFAULT 0,
  lon     NUMERIC                         DEFAULT NULL,
  lat     NUMERIC                         DEFAULT NULL,

  CONSTRAINT driver_pkey PRIMARY KEY (id),
  CONSTRAINT driver_phone_ukey UNIQUE (phone)
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.transport
(
  id        BIGSERIAL         NOT NULL,
  driver_id BIGINT            NOT NULL,
  name      CHARACTER VARYING NOT NULL,
  tariff    BIGINT            NOT NULL,
  cost      BIGINT            NOT NULL,
  carryng   NUMERIC           NOT NULL,

  CONSTRAINT transport_pkey PRIMARY KEY (id),
  CONSTRAINT fk_transport_driver_id FOREIGN KEY (driver_id)
  REFERENCES public.driver (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.offer
(
  id         BIGSERIAL                   NOT NULL,
  order_id   BIGINT                      NOT NULL,
  driver_id  BIGINT                      NOT NULL,
  price      BIGINT                      NOT NULL,
  dt_created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  dt_updated TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),

  CONSTRAINT offer_pkey PRIMARY KEY (id),
  CONSTRAINT uk_offer UNIQUE (order_id, driver_id),
  CONSTRAINT fk_offer_driver_id FOREIGN KEY (driver_id)
  REFERENCES public.driver (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  CONSTRAINT fk_offer_order_id FOREIGN KEY (order_id)
  REFERENCES public.order (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.order_deal
(
  id         BIGSERIAL                   NOT NULL,
  order_id   BIGINT                      NOT NULL,
  offer_id   BIGINT                      NOT NULL,
  driver_id  BIGINT                      NOT NULL,
  price      BIGINT                      NOT NULL,
  rating     NUMERIC                              DEFAULT NULL,
  completed  BOOLEAN                     NOT NULL DEFAULT FALSE,
  dt_created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  dt_updated TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),

  CONSTRAINT order_deal_pkey PRIMARY KEY (id),
  CONSTRAINT order_deal_ukey UNIQUE (order_id, offer_id, driver_id),
  CONSTRAINT fk_deal_order_id FOREIGN KEY (order_id)
  REFERENCES public.order (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  CONSTRAINT fk_deal_offer_id FOREIGN KEY (offer_id)
  REFERENCES public.offer (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  CONSTRAINT fk_deal_driver_id FOREIGN KEY (driver_id)
  REFERENCES public.driver (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE
)
WITH (
OIDS = FALSE
);

-- +mig Down

DROP TABLE public.order_deal CASCADE;
DROP TABLE public.offer CASCADE;
DROP TABLE public.transport CASCADE;
DROP TABLE public.driver CASCADE;
DROP TABLE public.region CASCADE;
DROP TABLE public.order CASCADE;

