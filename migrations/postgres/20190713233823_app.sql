-- +mig Up

CREATE TABLE public.app
(
  id SERIAL NOT NULL,
  status CHARACTER VARYING (16) NOT NULL,
  access_token CHARACTER VARYING (64) NOT NULL,
  dt_last_usage TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,

  CONSTRAINT app_pkey PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);



-- +mig Down

DROP TABLE public.app CASCADE ;

